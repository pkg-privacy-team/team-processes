This is a repository for documents internal to the pkg-privacy-maintainer team.

- Accepted proposals are stored in /.
- Draft proposals go in drafts/ until voted for or against.
- Rejected proposals go to rejected/.
- Amendments to existing proposals should be made in a dedicated branch and
  then sent to the mailing list.

Please use markdown for all proposals.
